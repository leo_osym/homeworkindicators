﻿namespace UserControlsLibrary
{
    partial class RotaryKnob
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // timer
            // 
            this.timer.Interval = 500;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // RotaryKnob
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.DoubleBuffered = true;
            this.Name = "RotaryKnob";
            this.Click += new System.EventHandler(this.RotaryKnob_Click);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.RotaryKnob_Paint);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.RotaryKnob_MouseDown);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.RotaryKnob_MouseUp);
            this.Resize += new System.EventHandler(this.RotaryKnob_Resize);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer;
    }
}
