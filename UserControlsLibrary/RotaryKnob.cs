﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace UserControlsLibrary
{
    public partial class RotaryKnob : UserControl
    {
        [Description("Min value"), Category("Behavior"), DefaultValue(0), Browsable(true),
         EditorBrowsable(EditorBrowsableState.Always)]
        public int Min { get; set; } = 0;

        [Description("Max value"), Category("Behavior"), DefaultValue(10), Browsable(true),
         EditorBrowsable(EditorBrowsableState.Always)]
        public int Max { get; set; } = 10;

        [Description("Default value"), Category("Behavior"), DefaultValue(0), Browsable(true),
         EditorBrowsable(EditorBrowsableState.Always)]
        public int Value { get; set; }
        public bool afterCenter = false;

        public RotaryKnob()
        {
            this.Width = this.Height;
            InitializeComponent();
            Invalidate();
        }

        private void RotaryKnob_Resize(object sender, EventArgs e)
        {
            this.Width = this.Height;
        }

        private void RotaryKnob_Paint(object sender, PaintEventArgs e)
        {
            if (Value >= (Max - Min) / 2) afterCenter = true;
            else afterCenter = false;
            Point centerPoint = new Point(ClientSize.Width/2, ClientSize.Height/2);
            Graphics gr = e.Graphics;
            GraphicsContainer container =
                gr.BeginContainer(
                    new Rectangle(centerPoint.X, centerPoint.Y, ClientSize.Width, ClientSize.Height),
                    new Rectangle(0, 0, ClientSize.Width, ClientSize.Height),
                    GraphicsUnit.Pixel);

            gr.RotateTransform(210);
            for (int i = Min; i <= Max; i++)
            {
                gr.RotateTransform(270/(Max-Min));
                gr.DrawLine(new Pen(Color.Red,2F), 0, -57, 0, -52);

                Font drawFont = new Font("Arial", 8);
                SolidBrush drawBrush = new SolidBrush(Color.Black);
                StringFormat format = new StringFormat();
                format.LineAlignment = StringAlignment.Center;
                format.Alignment = StringAlignment.Center;
                gr.DrawString(i%5 == 0 ? i.ToString() : "", drawFont, drawBrush, 0, -60, format);
            }
            gr.RotateTransform(90 + Value*270/(Max-Min));
            gr.DrawEllipse(Pens.Black, -50, -50,
                100, 100);
            gr.DrawEllipse(Pens.Black, -10, -50,
                20, 20);


            gr.EndContainer(container);
        }
        public void FormReload()
        {
            Invalidate();
        }

        private void RotaryKnob_Click(object sender, EventArgs e)
        {
            //MessageBox.Show($"Cursor position X: {PointToClient(Cursor.Position).X}, Cursor position Y: {PointToClient(Cursor.Position).Y},\n H{this.Height}, W{this.Width}");
            if (PointToClient(Cursor.Position).X >= 69)
                Value = Value < Max ? Value + 1 : Max;
            if (PointToClient(Cursor.Position).X < 69)
                Value = Value > Min ? Value - 1 : Min;
            OnValueChanged(new ValueChangedEventArgs(Value));
            FormReload();
        }

        // Added interesting control option: if mouse is presed on the left side of the user control - 
        // knob rotates to the left; otherwise if mouse is pressed on the right side of user control - 
        // rotates to the right
        private bool isMouseUp = false;
        private void RotaryKnob_MouseDown(object sender, MouseEventArgs e)
        {
            timer.Start();
            isMouseUp = true;
        }

        private void RotaryKnob_MouseUp(object sender, MouseEventArgs e)
        {
            isMouseUp = false;
            timer.Stop();
            isMouseUp = false;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if(isMouseUp == true)
            {
                if (PointToClient(Cursor.Position).X >= 69)
                    Value = Value < Max ? Value + 1 : Max;
                if (PointToClient(Cursor.Position).X < 69)
                    Value = Value > Min ? Value - 1 : Min;
                OnValueChanged(new ValueChangedEventArgs(Value));
                FormReload();
            }
        }

        #region Add ValueChanged event
        // delegate
        public delegate void ValueChangedEventHandler(object sender, ValueChangedEventArgs e);
        // Declare an event
        [Category("Action")]
        [Description("Fires when the value is changed")]
        public event ValueChangedEventHandler ValueChanged;
        protected virtual void OnValueChanged(ValueChangedEventArgs e)
        {
            // Raise the event
            ValueChanged?.Invoke(this, e);
        }
        public class ValueChangedEventArgs : EventArgs
        {
            public int NewValue { get; set; }

            public ValueChangedEventArgs(int newValue)
            {
                NewValue = newValue;
            }
        }
        #endregion
    }
}
