﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace UserControlsLibrary
{
    public enum IndicatorType { Left, Right, Both };
    public partial class ArrowIndicator : UserControl
    {
        [Description("Min value"), Category("Behavior"), DefaultValue(0), Browsable(true), 
         EditorBrowsable(EditorBrowsableState.Always)]
        public int Min { get; set; }

        [Description("Max value"), Category("Behavior"), DefaultValue(10), Browsable(true),
         EditorBrowsable(EditorBrowsableState.Always)]
        public int Max { get; set; } = 10;

        [Description("Default value"), Category("Behavior"), DefaultValue(2), Browsable(true),
         EditorBrowsable(EditorBrowsableState.Always)]
        public int Value { get; set; }

        [Description("Tick frequency"), Category("Behavior"), DefaultValue(1), Browsable(true),
         EditorBrowsable(EditorBrowsableState.Always)]
        public int TickFrequency { get; set; } = 1;

        [Description("Indicator type"), Category("Behavior"), Browsable(true),
         EditorBrowsable(EditorBrowsableState.Always)]
        public IndicatorType Type { get; set; } = IndicatorType.Left;

        private int preValue;

        [Browsable(true)]
        public ArrowIndicator()
        {
            FormResize();
            InitializeComponent();
            Invalidate();
        }

        private void ArrowIndicator_Paint(object sender, PaintEventArgs e)
        {
            Point centerPoint;
            int valangle, ang;
            Graphics gr = e.Graphics;
            CheckIndicatorType(out centerPoint, out valangle, out ang);
            gr.DrawRectangle(Pens.Black, 3, 3, ClientSize.Width - 7, ClientSize.Height - 7);
            int angle = 0;
            for (int i = Min; i <= Max; i++)
            {
                angle = Type == IndicatorType.Right ? ang * i + 90 : ang * i;
                gr.DrawLine(new Pen(Color.Black, 2f),
                    new PointF
                    {
                        X = centerPoint.X - (ClientSize.Height - 30) * (float)Math.Cos(angle * 2 * Math.PI / 360.0),
                        Y = centerPoint.Y - (ClientSize.Height - 30) * (float)Math.Sin(angle * 2 * Math.PI / 360.0),
                    },
                    new PointF
                    {
                        X = centerPoint.X - (ClientSize.Height - 25) * (float)Math.Cos(angle * 2 * Math.PI / 360.0),
                        Y = centerPoint.Y - (ClientSize.Height - 25) * (float)Math.Sin(angle * 2 * Math.PI / 360.0),
                    });

                DrawNumbers(centerPoint, gr, angle, i);
            }
            
            DrawArrow(centerPoint, valangle, gr);
        }

        private void DrawArrow(Point centerPoint, int valangle, Graphics gr)
        {
            gr.DrawLine(new Pen(Color.Blue, 2f),
                            new PointF
                            {
                                X = centerPoint.X - (ClientSize.Height - 35) * (float)Math.Cos(valangle * 2 * Math.PI / 360.0),
                                Y = centerPoint.Y - (ClientSize.Height - 35) * (float)Math.Sin(valangle * 2 * Math.PI / 360.0),
                            },
                            new PointF
                            {
                                X = centerPoint.X,
                                Y = centerPoint.Y,
                            }
                        );
        }

        private void DrawNumbers(Point centerPoint, Graphics gr, int angle, int i)
        {
            Font drawFont = new Font("Arial", 7);
            SolidBrush drawBrush = new SolidBrush(Color.Black);
            StringFormat format = new StringFormat();
            format.LineAlignment = StringAlignment.Center;
            format.Alignment = StringAlignment.Center;
           
            gr.DrawString((i % 5 == 0 & Type != IndicatorType.Right ? i.ToString() : ""),
                drawFont, drawBrush,
                centerPoint.X - (ClientSize.Height - 20) * (float)Math.Cos(angle * 2 * Math.PI / 360.0),
                centerPoint.Y - (ClientSize.Height - 20) * (float)Math.Sin(angle * 2 * Math.PI / 360.0),
                format);
            // for right-type indicator
            gr.DrawString((i % 5 == 0 & Type == IndicatorType.Right ? (Max - i).ToString() : ""),
                drawFont, drawBrush,
                centerPoint.X - (ClientSize.Height - 20) * (float)Math.Cos(angle * 2 * Math.PI / 360.0),
                centerPoint.Y - (ClientSize.Height - 20) * (float)Math.Sin(angle * 2 * Math.PI / 360.0),
                format);
        }

        private void CheckIndicatorType(out Point centerPoint, out int valangle, out int ang)
        {
            centerPoint = new Point(0, 0);
            valangle = 0;
            ang = 0;
            if (Value > Max) Value = Max;
            if (Value < Min) Value = Min;
            if (Value % TickFrequency == 0 | Value == Max | Value == Min) preValue = Value;

            
            if (Type == IndicatorType.Right)
            {
                centerPoint = new Point(7, ClientSize.Height - 7);
                valangle = ((Max - preValue) * 90 / (Max - Min)) + 90;
                ang = 90 / (Max - Min);
            }
            if (Type == IndicatorType.Left)
            {
                centerPoint = new Point(ClientSize.Width - 7, ClientSize.Height - 7);
                valangle = ((preValue) * 90 / (Max - Min));
                ang = 90 / (Max - Min);
            }
            if (Type == IndicatorType.Both)
            {
                this.Width = this.Height * 2;
                centerPoint = new Point(ClientSize.Width / 2, ClientSize.Height - 7);
                valangle = ((preValue) * 180 / (Max - Min));
                ang = 180 / (Max - Min);
            }
        }

        private void ArrowIndicator_Resize(object sender, EventArgs e)
        {
            FormResize();
        }

        private void FormResize()
        {
            if (Type == IndicatorType.Left || Type == IndicatorType.Right)
                this.Width = this.Height;
            else
                this.Width = this.Height * 2;
        }

        public void FormReload()
        {
            Invalidate();
        }
    }
}
