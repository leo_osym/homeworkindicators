﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Homework_ArrowIndicator
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void rotaryKnob1_ValueChanged(object sender, UserControlsLibrary.RotaryKnob.ValueChangedEventArgs e)
        {
            arrowIndicator2.Value = rotaryKnob1.Value;
            arrowIndicator2.FormReload();
            arrowIndicator1.Value = rotaryKnob1.Value;
            arrowIndicator1.FormReload();
            arrowIndicator3.Value = rotaryKnob1.Value;
            arrowIndicator3.FormReload();
        }
    }
}
