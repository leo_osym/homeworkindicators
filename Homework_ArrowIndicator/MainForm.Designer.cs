﻿namespace Homework_ArrowIndicator
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rotaryKnob1 = new UserControlsLibrary.RotaryKnob();
            this.arrowIndicator3 = new UserControlsLibrary.ArrowIndicator();
            this.arrowIndicator1 = new UserControlsLibrary.ArrowIndicator();
            this.arrowIndicator2 = new UserControlsLibrary.ArrowIndicator();
            this.SuspendLayout();
            // 
            // rotaryKnob1
            // 
            this.rotaryKnob1.Location = new System.Drawing.Point(12, 203);
            this.rotaryKnob1.Max = 20;
            this.rotaryKnob1.Name = "rotaryKnob1";
            this.rotaryKnob1.Size = new System.Drawing.Size(150, 150);
            this.rotaryKnob1.TabIndex = 5;
            this.rotaryKnob1.ValueChanged += new UserControlsLibrary.RotaryKnob.ValueChangedEventHandler(this.rotaryKnob1_ValueChanged);
            // 
            // arrowIndicator3
            // 
            this.arrowIndicator3.Location = new System.Drawing.Point(379, 25);
            this.arrowIndicator3.Name = "arrowIndicator3";
            this.arrowIndicator3.Size = new System.Drawing.Size(300, 150);
            this.arrowIndicator3.TabIndex = 4;
            this.arrowIndicator3.Type = UserControlsLibrary.IndicatorType.Both;
            this.arrowIndicator3.Value = 0;
            // 
            // arrowIndicator1
            // 
            this.arrowIndicator1.Location = new System.Drawing.Point(12, 25);
            this.arrowIndicator1.Max = 15;
            this.arrowIndicator1.Name = "arrowIndicator1";
            this.arrowIndicator1.Size = new System.Drawing.Size(150, 150);
            this.arrowIndicator1.TabIndex = 3;
            this.arrowIndicator1.TickFrequency = 2;
            this.arrowIndicator1.Type = UserControlsLibrary.IndicatorType.Left;
            this.arrowIndicator1.Value = 0;
            // 
            // arrowIndicator2
            // 
            this.arrowIndicator2.Location = new System.Drawing.Point(193, 25);
            this.arrowIndicator2.Max = 15;
            this.arrowIndicator2.Name = "arrowIndicator2";
            this.arrowIndicator2.Size = new System.Drawing.Size(150, 150);
            this.arrowIndicator2.TabIndex = 2;
            this.arrowIndicator2.Type = UserControlsLibrary.IndicatorType.Right;
            this.arrowIndicator2.Value = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.rotaryKnob1);
            this.Controls.Add(this.arrowIndicator3);
            this.Controls.Add(this.arrowIndicator1);
            this.Controls.Add(this.arrowIndicator2);
            this.Name = "MainForm";
            this.Text = "Demo Indicators";
            this.ResumeLayout(false);

        }

        #endregion
        private UserControlsLibrary.ArrowIndicator arrowIndicator2;
        private UserControlsLibrary.ArrowIndicator arrowIndicator1;
        private UserControlsLibrary.ArrowIndicator arrowIndicator3;
        private UserControlsLibrary.RotaryKnob rotaryKnob1;
    }
}

